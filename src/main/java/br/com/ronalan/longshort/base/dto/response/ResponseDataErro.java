package br.com.ronalan.longshort.base.dto.response;

public class ResponseDataErro {
	private String mensagem;
	private ListaErroEnum tipo;

	public ResponseDataErro(String mensagem, ListaErroEnum tipo) {
		this.mensagem = mensagem;
		this.tipo = tipo;
	}

	public String getErro() {
		return mensagem;
	}

	public ListaErroEnum getTipo() {
		return tipo;
	}

}
