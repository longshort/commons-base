package br.com.ronalan.longshort.base.gateway;

public interface SalvarGateway<T> {

	T salvar(T t);

}
