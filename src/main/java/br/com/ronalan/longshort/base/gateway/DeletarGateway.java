package br.com.ronalan.longshort.base.gateway;

public interface DeletarGateway<ID> {

	Boolean deletar(ID id);

}
