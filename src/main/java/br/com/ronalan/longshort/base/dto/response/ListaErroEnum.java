package br.com.ronalan.longshort.base.dto.response;

public enum ListaErroEnum {
	CAMPOS_OBRIGATORIOS, 
	DUPLICIDADE, 
	ENTIDADE_NAO_ENCONTRADA, 
	NAO_FOI_POSSIVEL_DELETAR, 
	TIMEOUT, 
	OUTROS;
}
