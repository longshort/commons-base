package br.com.ronalan.longshort.base.gateway;

import java.util.Optional;

public interface BuscarPorIdGateway<T,ID> {

	Optional<T> buscarPorId(ID id);

}
