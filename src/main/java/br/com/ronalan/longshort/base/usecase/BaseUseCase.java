package br.com.ronalan.longshort.base.usecase;

public interface BaseUseCase<I,O> {
	O executar(I input);
}
