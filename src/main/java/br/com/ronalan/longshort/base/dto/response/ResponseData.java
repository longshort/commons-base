package br.com.ronalan.longshort.base.dto.response;

import java.util.ArrayList;
import java.util.List;

public class ResponseData {
	
	private List<String> warnings = new ArrayList<>();
	private List<ResponseDataErro> erros = new ArrayList<>();
	private List<String> infos = new ArrayList<>();

	public void adicionarWarning(String... mensagens) {
		for (String msg : mensagens) {
			warnings.add(msg);
		}
	}

	public void adicionarErro(ResponseDataErro... mensagens) {
		for (ResponseDataErro msg : mensagens) {
			erros.add(msg);
		}
	}

	public void adicionarInfo(String... mensagens) {
		for (String msg : mensagens) {
			infos.add(msg);
		}
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public List<ResponseDataErro> getErros() {
		return erros;
	}

	public List<String> getInfos() {
		return infos;
	}
	
}
